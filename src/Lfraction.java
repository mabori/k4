import java.math.BigDecimal;
import java.math.RoundingMode;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {


   /** Main method. Different tests. */
   public static void main (String[] param) {
      new Lfraction(2, 4);
   }

   private long a;
   private long b;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      this.a = a;
      this.b = b;
      if (b < 0L) {
         this.a = - this.a;
         this.b = - this.b;
      } else if (b == 0L) {
         throw new RuntimeException("Denominator cannot be set to 0.");
      }
      long[] s = getSimplifiedLongs(this.a, this.b);
      this.a = s[0];
      this.b = s[1];
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.a;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.b;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return a + "/" + b;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m instanceof Lfraction) {
         Lfraction other = (Lfraction) m;
         return compareTo(other) == 0;
      }
      return false;
   }

   public Lfraction pow(int p) {
      if (p == 0)
         return new Lfraction(1, 1);
      if (p == 1)
         return this;
      if (p == -1)
         return inverse();
      if (p < -1)
         return times(pow(Math.abs(p) - 1)).inverse();
      return times(pow(p - 1));
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      Lfraction simplified = simplify();

      BigDecimal bigA = new BigDecimal(simplified.a)
              .setScale(32, RoundingMode.HALF_UP);
      BigDecimal bigB = new BigDecimal(simplified.b);
      return bigA.divide(bigB, RoundingMode.HALF_UP).hashCode();
   }

   private Lfraction simplify() {
      long[] simplified = getSimplifiedLongs(this.a, this.b);
      return new Lfraction(simplified[0], simplified[1]);
   }
   private static long[] getSimplifiedLongs(long a, long b) {
      long d = greatestCommonDenominator(a, b);
      return new long[] {a / d, b / d};
   }

   private static long greatestCommonDenominator(long a, long b) {
      if (b == 0)
         return a;
      return greatestCommonDenominator(b, Math.abs(a) % b);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      return new Lfraction(this.a * m.b + m.a * this.b,
              this.b * m.b);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(this.a * m.a, this.b * m.b);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (this.a == 0)
         throw new IllegalArgumentException("Cannot inverse a fraction equal to 0.");
      return new Lfraction(this.b, this.a);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(- this.a, this.b);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.a == 0)
         throw new IllegalArgumentException("Cannot divide by 0.");
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      Lfraction difference = minus(m);
      if (difference.a == 0L)
         return 0;
      return difference.a < 0 ? -1 : 1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.a, this.b);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.a / this.b;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(this.a % this.b, this.b);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) a / (double) b;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      if (d <= 0)
         throw new RuntimeException("Denominator has to be positive.");
      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (!s.contains("/"))
         throw new IllegalArgumentException("Slash symbol (\"/\") not found in following fraction representation: " + s);
      String[] split = s.split("/");
      for (String value : split) {
         char[] chars = value.trim().toCharArray();
         for (char aChar : chars) {
            if (!Character.isDigit(aChar) && aChar != '+' && aChar != '-')
               throw new IllegalArgumentException("Following fraction representation is not fully numeric: " + s);
         }
      }
      return new Lfraction(Long.parseLong(split[0].trim()), Long.parseLong(split[1].trim()));
   }
}

